package com.example.pauline.githubretrofit.api.chucknorrisAPI

import retrofit2.Call
import retrofit2.http.GET

interface ChuckNorrisClient{

    @GET("/jokes/random")
    fun getRandomJoke(): Call<ChuckNorrisJoke>

}