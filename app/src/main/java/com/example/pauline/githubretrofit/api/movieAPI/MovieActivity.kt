package com.example.pauline.githubretrofit.api.movieAPI

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.example.pauline.githubretrofit.R
import com.example.pauline.githubretrofit.displayImage.DownloadImageTask
import kotlinx.android.synthetic.main.activity_movie.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MovieActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        searchMovie.setOnClickListener {
            if (!checkInput(movieTitleInput)){
                return@setOnClickListener
            } else {
                val mTitle = movieTitleInput.text.toString()

                val API_BASE_URL = "http://www.omdbapi.com/"
                val APIKEY = "de1e3cf"

                val builder = Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        )

                val retrofit = builder.build()

                // Create a very simple REST adapter which points the Pokemon API endpoint.
                val client = retrofit.create(MovieClient::class.java)

                // Fetch a list of the Pokemon repositories.
                val call = client.searchMovieByTitle(mTitle, APIKEY)

                call.enqueue(object : Callback<Movie> {
                    override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                        val movie = response.body()
                        if (movie!!.Poster != null) {
                            movieTitle.text = movie.Title
                            movieYear.text = movie.Year
                            movieRated.text = movie.Rated
                            movieGenre.text = movie.Genre
                            movieDirector.text = movie.Director
                            DownloadImageTask(moviePoster).execute(movie.Poster)
                        } else {
                            movieTitle.text = "No data"
                            movieYear.text = ""
                            movieRated.text = ""
                            movieGenre.text = ""
                            movieDirector.text = ""
                            moviePoster.setImageResource(android.R.color.transparent)
                        }
                    }

                    override fun onFailure(call: Call<Movie>, t: Throwable) {
                        Toast.makeText(baseContext, t.toString(), Toast.LENGTH_LONG).show()
                    }

                })
            }
        }
    }

    private fun checkInput(editText: EditText): Boolean {
        val input = editText.text.toString()
        if (input.trim().isEmpty()) {
            editText.error = "Input field"
            return false
        }
        return true
    }
}
