package com.example.pauline.githubretrofit.api.movieAPI

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieClient{

    @GET("/")
    fun searchMovieByTitle(
            @Query("t") movieTitle: String,
            @Query("apikey") apiKey: String
    ): Call<Movie>

}