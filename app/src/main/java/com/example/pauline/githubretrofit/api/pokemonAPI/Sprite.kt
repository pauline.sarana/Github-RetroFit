package com.example.pauline.githubretrofit.api.pokemonAPI

data class Sprite(
        val back_female: String? = null,
        val back_shiny_female: String? = null,
        val back_default: String? = null,
        val front_female: String? = null,
        val front_shiny_female: String? = null,
        val back_shiny: String? = null,
        val front_default: String? = null,
        val front_shiny: String? = null
)