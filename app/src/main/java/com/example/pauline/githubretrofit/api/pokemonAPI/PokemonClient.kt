package com.example.pauline.githubretrofit.api.pokemonAPI

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonClient{

    @GET("api/v2/pokemon/{name}")
    fun searchPokemonByName(
            @Path("name") pokemonName: String
    ): Call<Pokemon>

}