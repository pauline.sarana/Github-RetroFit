package com.example.pauline.githubretrofit.api.weatherAPI

data class Weather (
    val id: Int? = 0,
    val main: String? = null,
    val description: String? = null
)
