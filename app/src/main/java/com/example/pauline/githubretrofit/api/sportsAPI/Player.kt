package com.example.pauline.githubretrofit.api.sportsAPI

data class Player(
    val idPlayer: String? = null,
    val strNationality: String? = null,
    val strPlayer: String? = null,
    val strTeam: String? = null,
    val strSport: String? = null,
    val strBirthLocation: String? = null
)