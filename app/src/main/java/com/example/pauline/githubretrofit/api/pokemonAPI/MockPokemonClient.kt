package com.example.pauline.githubretrofit.api.pokemonAPI

import retrofit2.Call
import retrofit2.mock.BehaviorDelegate

class MockPokemonClient(val delegate: BehaviorDelegate<PokemonClient>): PokemonClient {
    override fun searchPokemonByName(pokemonName: String): Call<Pokemon> =
            delegate.returningResponse(Pokemon("Charizard", "179", "197", 350,
                    Sprite("3", "2", "3", "3", "3", "3", "https://images-na.ssl-images-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_SX300.jpg", "3")))
                    .searchPokemonByName(pokemonName)

}