package com.example.pauline.githubretrofit.api.nasaAPI

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.pauline.githubretrofit.R
import kotlinx.android.synthetic.main.activity_nasa.*

class NASAActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nasa)

        webView.loadUrl("http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000ML0044631200305217E01_DXXX.jpg")
    }
}
