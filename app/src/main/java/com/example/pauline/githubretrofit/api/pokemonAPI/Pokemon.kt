package com.example.pauline.githubretrofit.api.pokemonAPI

data class Pokemon(
    val name: String? = null,
    val height: String? = null,
    val weight: String? = null,
    val base_experience: Int? = 0,
    val sprites: Sprite? = null
)
