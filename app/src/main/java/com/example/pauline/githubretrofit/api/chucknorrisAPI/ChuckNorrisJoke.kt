package com.example.pauline.githubretrofit.api.chucknorrisAPI

data class ChuckNorrisJoke (
    val value: String? = null
)