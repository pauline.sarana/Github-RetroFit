package com.example.pauline.githubretrofit.api.weatherAPI

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.example.pauline.githubretrofit.R
import kotlinx.android.synthetic.main.activity_open_weather.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OpenWeatherActivity : AppCompatActivity() {

    private val API_BASE_URL = "http://api.openweathermap.org"
    private val APIKEY = "94af555678bca4f72b42baf75026eefa"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_weather)

        searchButton.setOnClickListener {

            if (!checkInput(cityName)) {
                return@setOnClickListener
            } else {
                val cname = cityName.text.toString()

                val builder = Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        )

                val retrofit = builder.build()

                val client = retrofit.create(OpenWeatherClient::class.java)

                val call = client.getWeather(cname, APIKEY)

                call.enqueue(object : Callback<WeatherData> {

                    override fun onResponse(call: Call<WeatherData>?, response: Response<WeatherData>) {
                        val res = response.body()
                        if (res != null) {
                            city.text = res.name + ", " + res.sys!!.country
                            val text = res.weather!![0].main + " " + res.weather[0].description
                            weather.text = text
                            val mainText = "Temperature: " + res.main!!.temp + "\nPressure: " + res.main.pressure + "\nHumidity: " + res.main.humidity
                            main.text = mainText
                        } else {
                            city.text = ""
                            weather.text = "No value"
                            main.text = ""
                        }
                    }

                    override fun onFailure(call: Call<WeatherData>?, t: Throwable?) {
                        Toast.makeText(baseContext, t.toString(), Toast.LENGTH_LONG).show()
                    }

                })
            }
        }
    }

    private fun checkInput(editText: EditText): Boolean {
        val input = editText.text.toString()
        if (input.trim().isEmpty()) {
            editText.error = "Input field"
            return false
        }
        return true
    }
}
