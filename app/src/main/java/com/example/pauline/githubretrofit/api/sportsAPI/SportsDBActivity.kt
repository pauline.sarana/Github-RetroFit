package com.example.pauline.githubretrofit.api.sportsAPI

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.pauline.githubretrofit.R
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



class SportsDBActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sports_db)

        val API_BASE_URL = "http://www.thesportsdb.com/"
        val APPID = "1"

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor({ chain ->
                    val original = chain.request()
                    val originalHttpUrl = original.url()

                    Log.e("SportsDBActivity", originalHttpUrl.toString())

                    // Request customization: add request headers
                    val requestBuilder = original.newBuilder()
                            .url(originalHttpUrl)

                    val request = requestBuilder.build()
                    chain.proceed(request)
                })
                .build()

        val builder = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(
                        GsonConverterFactory.create()
                )

        val retrofit = builder.build()

        // Create a very simple REST adapter which points the SportsDB API endpoint.
        val client = retrofit.create(TheSportsDBClient::class.java)


        // Fetch a list of the players
        val call = client.searchPlayers(APPID, "Arsenal")

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(object : Callback<Players> {
            override fun onResponse(call: Call<Players>, response: Response<Players>) {
                val players = response.body()
                if (players == null) {
                    Toast.makeText(this@SportsDBActivity, "NULL :(", Toast.LENGTH_SHORT).show()
                } else{
//                    playerName.text = players.players!![0].strPlayer
//                    playerBirth.text = players.players[0].strBirthLocation
//                    playerNationality.text = players.[0].strNationality
//                    playerSport.text = players.players[0].strSport
//                    playerTeam.text = players.players[0].strTeam
                }
            }

            override fun onFailure(call: Call<Players>, t: Throwable) {
                Toast.makeText(this@SportsDBActivity, "error :(", Toast.LENGTH_SHORT).show()
            }

        })
    }
}
