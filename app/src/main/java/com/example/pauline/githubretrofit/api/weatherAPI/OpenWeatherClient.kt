package com.example.pauline.githubretrofit.api.weatherAPI

import com.example.pauline.githubretrofit.api.weatherAPI.WeatherData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherClient{

    @GET("/data/2.5/weather")
    fun getWeather(
            @Query("q") cityName: String,
            @Query("APPID") appId: String
    ): Call<WeatherData>

}