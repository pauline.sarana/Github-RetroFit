package com.example.pauline.githubretrofit.api.sportsAPI

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheSportsDBClient{

    @GET("/api/v1/json/{apikey}/searchplayers.php")
    fun searchPlayers(
            @Path("apikey") appID: String,
            @Query("t") teamName: String
    ): Call<Players>

}