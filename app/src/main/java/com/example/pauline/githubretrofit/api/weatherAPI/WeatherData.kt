package com.example.pauline.githubretrofit.api.weatherAPI

data class WeatherData (
        val name: String? = null,
        val weather: ArrayList<Weather>? = null,
        val main: MainWeather? = null,
        val sys: System? = null
)
