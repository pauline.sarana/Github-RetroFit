package com.example.pauline.githubretrofit.api.sportsAPI

import com.example.pauline.githubretrofit.api.sportsAPI.Player

data class Players(
    val players: ArrayList<Player>? = null
)