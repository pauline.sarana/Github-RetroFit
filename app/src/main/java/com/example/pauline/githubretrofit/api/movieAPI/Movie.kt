package com.example.pauline.githubretrofit.api.movieAPI

data class Movie(
    val Title: String? = null,
    val Year: String? = null,
    val Rated: String? = null,
    val Genre: String? = null,
    val Director: String? = null,
    val Poster: String? = null
)