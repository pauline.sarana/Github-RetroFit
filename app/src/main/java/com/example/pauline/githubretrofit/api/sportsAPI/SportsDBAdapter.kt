package com.example.pauline.githubretrofit.api.sportsAPI

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.pauline.githubretrofit.R
import com.example.pauline.githubretrofit.api.sportsAPI.Player

class SportsDBAdapter(context: Context, private val values: List<Player>) : ArrayAdapter<Player>(context, R.layout.list_item_pagination, values) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var row = convertView

        if (row == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            row = inflater.inflate(R.layout.list_item_pagination, parent, false)
        }

        val textView = row!!.findViewById(R.id.list_item_pagination_text) as TextView

        val item = values[position]
        val message = item.strPlayer
        textView.text = message

        return row
    }
}