package com.example.pauline.githubretrofit.api.weatherAPI

data class System(
    val country: String? = null
)