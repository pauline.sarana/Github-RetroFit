package com.example.pauline.githubretrofit.api.github

data class GitHubRepo (
    val id: Int = 0,
    val name: String? = null
)