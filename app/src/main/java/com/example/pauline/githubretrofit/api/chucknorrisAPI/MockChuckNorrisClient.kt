package com.example.pauline.githubretrofit.api.chucknorrisAPI

import retrofit2.Call
import retrofit2.mock.BehaviorDelegate

class MockChuckNorrisClient(val delegate: BehaviorDelegate<ChuckNorrisClient>): ChuckNorrisClient {
    override fun getRandomJoke(): Call<ChuckNorrisJoke> = delegate.returningResponse(ChuckNorrisJoke("Chuck Norris doesn't swim, water just wants to be around him.")).getRandomJoke()
}