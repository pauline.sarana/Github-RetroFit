package com.example.pauline.githubretrofit.api.github

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.pauline.githubretrofit.R
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.Toast
import com.example.pauline.githubretrofit.api.chucknorrisAPI.ChuckNorrisActivity
import com.example.pauline.githubretrofit.api.movieAPI.MovieActivity
import com.example.pauline.githubretrofit.api.sportsAPI.SportsDBActivity
import com.example.pauline.githubretrofit.api.weatherAPI.OpenWeatherActivity
import com.example.pauline.githubretrofit.api.nasaAPI.NASAActivity
import com.example.pauline.githubretrofit.api.pokemonAPI.PokemonActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gotoWeather.setOnClickListener {
            val intent = Intent(this, OpenWeatherActivity::class.java)
            startActivity(intent)
        }

        gotoSports.setOnClickListener {
            val intent =Intent(this, SportsDBActivity::class.java)
            startActivity(intent)
        }

        gotoPokemon.setOnClickListener {
            val intent = Intent(this, PokemonActivity::class.java)
            startActivity(intent)
        }

        gotoMovie.setOnClickListener {
            val intent = Intent(this, MovieActivity::class.java)
            startActivity(intent)
        }

        gotoChuckNorris.setOnClickListener {
            val intent = Intent(this, ChuckNorrisActivity::class.java)
            startActivity(intent)
        }

        gotoNASA.setOnClickListener {
            val intent = Intent(this, NASAActivity::class.java)
            startActivity(intent)
        }

        val API_BASE_URL = "https://api.github.com/"

        val builder = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(
                        GsonConverterFactory.create()
                )

        val retrofit = builder.build()

        // Create a very simple REST adapter which points the GitHub API endpoint.
        val client = retrofit.create(GitHubClient::class.java)

        // Fetch a list of the Github repositories.
        val call = client.reposForUser("bootaXX")

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(object : Callback<List<GitHubRepo>> {
            override fun onResponse(call: Call<List<GitHubRepo>>, response: Response<List<GitHubRepo>>) {
                val repos = response.body()
                pagination_list.adapter = GitHubRepoAdapter(this@MainActivity, repos!!)
            }

            override fun onFailure(call: Call<List<GitHubRepo>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "error :(", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
