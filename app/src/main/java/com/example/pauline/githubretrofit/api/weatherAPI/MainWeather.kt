package com.example.pauline.githubretrofit.api.weatherAPI

data class MainWeather (
    val temp: String? = null,
    val pressure: String? = null,
    val humidity:String? = null
)