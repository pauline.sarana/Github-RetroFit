package com.example.pauline.githubretrofit.api.chucknorrisAPI

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.pauline.githubretrofit.R
import kotlinx.android.synthetic.main.activity_chuck_norris.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit

class ChuckNorrisActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chuck_norris)

        getJoke.setOnClickListener {
            val API_BASE_URL = "https://api.chucknorris.io/"

            val builder = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(
                            GsonConverterFactory.create()
                    )

            val retrofit = builder.build()

            val behavior = NetworkBehavior.create()
            behavior.setDelay(1000, TimeUnit.MILLISECONDS)
            behavior.setFailurePercent(50)
            behavior.setVariancePercent(50)

            // Create a very simple REST adapter which points the Pokemon API endpoint.
//            val client = retrofit.create(ChuckNorrisClient::class.java)

            val mockRetrofit = MockRetrofit.Builder(retrofit).networkBehavior(behavior).build()
            val delegate = mockRetrofit.create(ChuckNorrisClient::class.java)
            val client = MockChuckNorrisClient(delegate)

            // Fetch a list of the Pokemon repositories.
            val call = client.getRandomJoke()

            call.enqueue(object: Callback<ChuckNorrisJoke> {
                override fun onFailure(call: Call<ChuckNorrisJoke>, t: Throwable) {
                    Toast.makeText(baseContext, t.toString(), Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<ChuckNorrisJoke>, response: Response<ChuckNorrisJoke>) {
                    Toast.makeText(baseContext, "Boom", Toast.LENGTH_LONG).show()
                    val joke = response.body()
                    jokeText.text = joke!!.value
                }

            })
        }
    }
}
