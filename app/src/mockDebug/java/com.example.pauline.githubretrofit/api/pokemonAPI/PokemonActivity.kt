package com.example.pauline.githubretrofit.api.pokemonAPI

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.example.pauline.githubretrofit.R
import com.example.pauline.githubretrofit.displayImage.DownloadImageTask
import kotlinx.android.synthetic.main.activity_pokemon.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit

class PokemonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon)

        searchPokemon.setOnClickListener {

            if (!checkInput(pokemonName)){
                return@setOnClickListener
            } else {
                val pname = pokemonName.text.toString()

                val API_BASE_URL = "http://pokeapi.co/"

                val builder = Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        )

                val retrofit = builder.build()

                // Create a very simple REST adapter which points the Pokemon API endpoint.
//                val client = retrofit.create(PokemonClient::class.java)

                val mockretrofit = MockRetrofit.Builder(retrofit).build()
                val delegate = mockretrofit.create(PokemonClient::class.java)
                val client = MockPokemonClient(delegate)

                // Fetch a list of the Pokemon repositories.
                val call = client.searchPokemonByName(pname)

                call.enqueue(object : Callback<Pokemon> {
                    override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                        val pokemon = response.body()
                        if (pokemon != null) {
                            pokeName.text = pokemon.name
                            pokeHeight.text = "Height: " + pokemon.height + "inches"
                            pokeWeight.text = "Weight: " + pokemon.weight + "pokegrams"
                            pokeForm.text = "Base Experience: " + pokemon.base_experience
                            DownloadImageTask(pokeSprite).execute(pokemon.sprites!!.front_default)
                        } else {
                            pokeName.text = "No data"
                            pokeHeight.text = ""
                            pokeWeight.text = ""
                            pokeForm.text = ""
//                            pokeSprite.setImageResource(android.R.color.transparent)
                        }
                    }

                    override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                        Toast.makeText(baseContext, t.toString(), Toast.LENGTH_LONG).show()
                    }
                })
            }
        }
    }

    private fun checkInput(editText: EditText): Boolean {
        val input = editText.text.toString()
        if (input.trim().isEmpty()) {
            editText.error = "Input field"
            return false
        }
        return true
    }
}
